;; Publish the contents of this repository as a HTML page.
;; Source: Rasmus

(require 'package)
(package-initialize)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'org-plus-contrib)
(package-install 'htmlize)

(require 'org)
(require 'ox-publish)

;;(setq org-latex-pdf-process (list "latexmk -pdflatex='lualatex -shell-escape -interaction nonstopmode' -pdf -f  %f"))
(setq org-latex-pdf-process (list "pdflatex -shell-escape -interaction nonstopmode %f" "bibtex %b" "pdflatex -shell-escape -interaction nonstopmode %f" "pdflatex -shell-escape -interaction nonstopmode %f"))

(setq org-confirm-babel-evaluate nil)


(defvar site-attachments
  (regexp-opt '("m" "py" "ipynb" "scm"
                "jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

;; Force publishing of unchanged files. Otherwise, missing files cause
;; 'Not Found' errors while browsing the generated website.
(setq org-publish-use-timestamps-flag nil)

(setq org-src-preserve-indentation t) ; Preserve indentation on export and tangle.
(setq org-src-tab-acts-natively t) ; Use TAB to indent code in code blocks of org mode.
(setq python-indent-guess-indent-offset t)

(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "."
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html org-latex-publish-to-pdf)
             :publishing-directory "./public"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\"
href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")
       (list "site-static"
             :base-directory "."
             :exclude "public/"
             :base-extension site-attachments
             :recursive t
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site-tangles"
             :base-directory "./"
             :recursive t
             :publishing-directory "./public"
             :recursive t
             :publishing-function 'org-babel-tangle-publish)
       (list "site" :components '("site-org"))))

(provide 'publish)
