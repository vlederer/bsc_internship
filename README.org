#+TITLE: BSC Internship: Build and Bench Maphys++ on Marenostrum 
#+DATE: {{{time(%d/%m/%Y)}}}
#+AUTHOR: Victor Lederer
#+EMAIL: victor.lederer@bsc.es
#+SETUPFILE: ./inria-org-html-themes/theme-readtheorginria.setup
#+OPTIONS: timestamp:nil email:t

This repository contains materials needed to build and bench Maphys++
on Marenostrum
* BSC Internship
:PROPERTIES:
:CUSTOM_ID: BSC Internship
:END:

- [[./TODO.org][TODO LIST]]

